<!-- @format -->

# Ajira web API assessment

This application is a nodeJS based application build using express. In order to install and run the following commands are mentioned below:

- Clone this repository in the local machine
- In the root directory, run 'npm install'
- Once all the dependecies are downloaded and installed the application is ready to run
- To run the application use the command 'npm run start:dev', this will run the app using nodemon

The end points with detail can be find at this URL: https://documenter.getpostman.com/view/1866411/TVev4QTq

In case that does not works, the APIs can be viewed at https://www.getpostman.com/collections/7413a37e38d90b870626
