import express from 'express';

let app1 = express();
app1.disable("x-powered-by");

let helmet = require("helmet");
var bodyParser = require('body-parser')

let app2 = express();
app2.use(helmet.hidePoweredBy());

const PORT = 8778;

// parse application/json
app2.use(bodyParser.json())

// app2.post('/devices', (req, res) => res.send('Express + TypeScript Server'));

app2.use('/ajiranet/process', require('./routes'));

app2.listen(PORT, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});