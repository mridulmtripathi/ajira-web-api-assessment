import { Connection } from "./models/ConnectionsCreateModel";
import { Device } from "./models/DevicesCreateModel";
var Graph = require("graph-data-structure");
var graph = Graph();

var devicesMap = new Map()

export function saveDevice(device: Device) {
    if (devicesMap.has(device.name)) {
        return false
    }

    devicesMap.set(device.name, device)
    graph.addNode(device.name);

    return true
}

export function saveConnections(connection: Connection) {
    console.log('connection', connection)

    var error = "";
    var device: Device = devicesMap.get(connection.source)
    if (device) {

        for (let i = 0; i < connection.targets.length; i++) {
            if (devicesMap.get(connection.targets[i])) {
                console.log('##device', connection.targets[i])
                let temp: Device = devicesMap.get(connection.targets[i])

                console.log('temp', temp)
                if (temp && temp.name != device.name) {
                    graph.addEdge(device.name, temp.name);
                    graph.addEdge(temp.name, device.name);
                }
                else {
                    error = error + " " + connection.targets[i]
                }
            }
            else {
                error = error + " " + connection.targets[i]
            }
        }
        return { status: "Entries added successfully", errorEntries: error }
    }

    return { error: "Source Device not found in system. Add this device first to add connections." }
}

export function getDevices() {
    var keyList = []

    for (let key of devicesMap.keys()) {
        keyList.push(key);
    }

    return { devices: keyList }
}

export function getRoutesInfo(from: any, to: any) {
    var tempFrom: Device = devicesMap.get(from)
    var tempTo: Device = devicesMap.get(to)

    if (tempFrom.name == tempTo.name) {
        return {
            path: [tempFrom.name, tempTo.name]
        }
    }

    if (tempFrom && tempTo) {
        if (tempFrom.type == 'COMPUTER' && tempTo.type == 'COMPUTER') {
            var path
            try {
                path = graph.shortestPath(from, to)
                return {
                    path: path
                }
            }
            catch {
                return {
                    error: "No path possible between the two entries"
                }
            }
        }
        else {
            return {
                error: "The start and end computers are not COMPUTERS but either one or both is/are repeater/s"
            }
        }
    }

    return {
        error: "The start and end computers are not in the system. Add them first to use it."
    }
}

export function updateDeviceStrength(device: any, strength: number) {
    var deviceDetails: Device = devicesMap.get(device)

    if (deviceDetails.type != 'COMPUTER') {
        return { error: "Cannot set strength for a repeater" }
    }

    if (deviceDetails) {
        if (!strength || strength < 0) {
            return {
                error: "Strength shuld be a number and cannot be negative"
            }
        }
        deviceDetails.strength = Number(strength)

        devicesMap.set(deviceDetails.name, deviceDetails)
    }

    return {
        device: deviceDetails,
        updated: true
    }
}