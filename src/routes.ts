import express, { Request, Response } from "express";
import { body, validationResult, query, param, check } from 'express-validator';
const { checkSchema } = require('express-validator/check');
import { Connection } from "./models/ConnectionsCreateModel";
import { Device } from "./models/DevicesCreateModel";
import { getDevices, getRoutesInfo, saveConnections, saveDevice, updateDeviceStrength } from './netService'
let router = express.Router();

var Schema = {
    "type": {
        in: 'body',
        matches: {
            options: [/\b(?:COMPUTER|REPEATER)\b/],
            errorMessage: "Invalid type"
        }
    }
}

router.post('/devices', [
    body('type').exists(),
    body('name').exists(),
    checkSchema(Schema)
], (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    var device: Device = req.body;
    if (device.type == 'COMPUTER') {
        device.strength = 5
    }

    if (saveDevice(device)) {
        return res.status(200).json(device);
    }
    else {
        return res.status(400).json({ error: 'Device Name already exists. Need an unique identifier' });
    }
});


router.post('/connections', [
    body('source').exists(),
    body('targets').exists()
], (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    var connection: Connection = req.body;

    return res.status(200).json(saveConnections(connection));
});

router.get('/devices', (req: Request, res: Response) => {
    return res.status(200).json(getDevices());
});

router.get('/info-routes', [
    query('from').exists().withMessage("Name of Start computer required"),
    // query('to').exists().withMessage("Name of End node computer required")
    check('to').exists()
], (req: Request, res: Response) => {
    return res.status(200).json(getRoutesInfo(req.query.from, req.query.to));
});

router.put('/devices/:device/strength', [
    body('value').exists(),
    param('device').exists()
], (req: Request, res: Response) => {

    var responseObject = updateDeviceStrength(req.params.device, req.body.value);
    if (responseObject.device) {
        return res.status(200).json(responseObject);
    }
    else {
        return res.status(400).json({ error: "The device not found in system. Add this device first to update the strength." });
    }
});

module.exports = router;