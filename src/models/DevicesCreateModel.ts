export class Device {
    type!: string
    name!: string
    strength!: number
}
